use anyhow::Result;
use lazy_static::lazy_static;
use regex::Regex;
use reqwest::Client;
use std::collections::HashSet;
use std::io::BufRead;
use std::{fs, io, path, time};
use tokio::sync::mpsc;
use url::Url;

fn req_client() -> Result<Client> {
    Ok(Client::builder()
        .user_agent("legoktm")
        .pool_idle_timeout(Some(time::Duration::from_secs(5)))
        .build()?)
}

async fn lists(client: &Client) -> Result<Vec<String>> {
    let regex = Regex::new("<a href=\"listinfo/(.*?)\"><strong>").unwrap();
    let body = client
        .get("https://lists.wikimedia.org/mailman/listinfo")
        .send()
        .await?
        .text()
        .await?;
    let matches: Vec<String> = regex
        .captures_iter(&body)
        .map(|cap| cap[1].to_string())
        .collect();

    Ok(matches)
}

async fn dates(client: &Client, list: &str) -> Result<Vec<String>> {
    let url = format!("https://lists.wikimedia.org/pipermail/{}/", list);
    eprintln!("Fetching {}...", &url);
    let body = client
        .get(&url)
        .send()
        .await?
        .text()
        .await?;
    let regex = Regex::new("<A href=\"(.*?)/date.html\">\\[ Date \\]</a>").unwrap();
    let matches: Vec<String> = regex
        .captures_iter(&body)
        .map(|cap| cap[1].to_string())
        .collect();

    Ok(matches)
}

async fn messages(client: &Client, list: &str, date: &str) -> Result<Vec<String>> {
    lazy_static! {
        static ref RE: Regex = Regex::new("<A HREF=\"(.*?).html\">").unwrap();
    }

    let url = format!(
            "https://lists.wikimedia.org/pipermail/{}/{}/date.html",
            list, date
        );
    let resp = match client.get(&url).send().await {
        Ok(resp) => resp,
        Err(err) => {
            eprintln!("Retrying {}, failed with: {}", &url, err.to_string());
            // Retry one more time...
            client.get(&url).send().await?
        }
    };
    let body = resp
        .text()
        .await?;
    let mut matches: Vec<String> = RE
        .captures_iter(&body)
        .map(|cap| cap[1].to_string())
        .collect();
    matches.reverse();

    Ok(matches)
}

async fn parse_message(client: &Client, url: &str) -> Result<String> {
    lazy_static! {
        static ref RE: Regex = Regex::new("<LINK REL=\"made\"\\s+HREF=\"(.*?)\">").unwrap();
    }
    let resp = match client.get(url).send().await {
        Ok(resp) => resp,
        Err(err) => {
            // Retry one more time
            eprintln!("Retrying {}, failed with: {}", url, err.to_string());
            client.get(url).send().await?
        }
    };
    let body = resp.text().await?;
    match RE.captures(&body) {
        Some(caps) => {
            let mailto = caps[1].to_string();
            let parsed = Url::parse(&mailto)?;
            for (key, val) in parsed.query_pairs() {
                if key == "In-Reply-To" {
                    return Ok(val
                        .to_string()
                        .trim_start_matches('<')
                        .trim_end_matches('>')
                        .to_string());
                }
            }

            eprintln!("No In-Reply-To found: {}", &url);
            Ok("".to_string())
        }
        None => panic!("Unable to parse URL from {}", &url),
    }
}

fn resume() -> Result<HashSet<String>> {
    let mut set = HashSet::new();
    if !path::Path::new("archives.csv.bak").exists() {
        return Ok(set);
    }
    let f = fs::File::open("archives.csv.bak")?;
    let f = io::BufReader::new(f);
    for line in f.lines() {
        let real_line = line?;
        let sp: Vec<&str> = real_line.split(',').collect();
        set.insert(sp[0].to_string());
    }
    eprintln!("Loaded {} items", &set.len());

    Ok(set)
}

#[tokio::main]
async fn main() -> Result<()> {
    let (mut tx, mut rx): (mpsc::Sender<String>, mpsc::Receiver<String>) = mpsc::channel(128);

    let manager = tokio::spawn(async move {
        let set = resume().unwrap();
        let client2 = req_client().unwrap();
        while let Some(pipermail) = rx.recv().await {
            if set.contains(&pipermail) {
                //eprintln!("Skipping {}", &pipermail);
                continue;
            }
            let in_reply_to = parse_message(&client2, &pipermail).await.unwrap();
            println!("{},{}", &pipermail, &in_reply_to);
        }
    });

    let client = req_client()?;
    for list in lists(&client).await? {
        for date in dates(&client, &list).await? {
            for message in messages(&client, &list, &date).await? {
                let pipermail = format!(
                    "https://lists.wikimedia.org/pipermail/{}/{}/{}.html",
                    &list, &date, &message
                );
                tx.send(pipermail).await?;
            }
        }
    }
    //dbg!(lists);
    manager.await?;
    Ok(())
}
